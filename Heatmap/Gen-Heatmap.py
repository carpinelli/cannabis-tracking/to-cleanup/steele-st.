#! /usr/bin/env python3

"""Fills out TGS Heatmap."""

import re
import json
from pathlib import Path

import xlrd
import pandas as pd
import openpyxl
from openpyxl.utils import get_column_interval

# from IPython.core.debugger import set_trace


ROOM_COL = "Location"
# USECOLS = [2, 6, 7, 10, 12]
USECOLS = [2, 6, 8, 12, 14]

Inventory_Filename = "PlantsInventoryReport.xls"
Metrc_Rooms_Filename = "data/METRC Rooms.json"
Heatmap_Filenames = ["data/Heatmap (Empty).xlsx",
                     "data/Old Heatmap (Empty).xlsx"]
Heatmap_Sheetname = "Heatmap"
Dimensions_Patterns = re.compile(r"(\w)(\d{1,2}):(\w)(\d{1,2})")
# 1=firstletter, 2=firstrow, 3=lastletter, 4=lastrow

# Get Date
wb = xlrd.open_workbook(Inventory_Filename)  # open inventory workbook
ws = wb.sheet_by_index(0)  # get the first/only worksheet
date_cell = ws.cell_value(2, 4)  # get date range from inventory report
date_cell = date_cell.split()  # get list split by whitespace
date = date_cell[3].replace('/', '.')
# 3rd word in cell is the most recent date

# Use Date Setup
Output_Filename = f"{date} Heatmap.xlsx"

# Load Rooms
Metrc_Rooms = []
with open(Metrc_Rooms_Filename) as json_file:
    Metrc_Rooms = json.load(json_file)

# Load Inventory
inventory = pd.read_excel(Inventory_Filename,
                          header=12,
                          usecols=USECOLS)
room_counts = inventory[ROOM_COL].value_counts()

for filename in Heatmap_Filenames:
    # Load Heatmap and Setup Openpyxl
    heatmap_wb = openpyxl.load_workbook(filename)
    heatmap = heatmap_wb[Heatmap_Sheetname]
    dimensions = heatmap.dimensions  # add re, for now: "A1:V43"
    match = Dimensions_Patterns.search(dimensions)
    if match is None:
        print(f"Error, no match for: {dimensions}")
        input("Press enter to exit...")
        exit(-1)
    firstletter = match.group(1)
    lastletter = match.group(3)
    firstrow = int(match.group(2))
    lastrow = int(match.group(4))
    columns = get_column_interval(firstletter, lastletter)

    # Copy Counts
    for row in range(firstrow, lastrow):
        for index, column in enumerate(columns):
            roomname = str(heatmap[column][row].value)
            if (roomname is None) or (roomname.isnumeric()):
                continue
            if filename == Heatmap_Filenames[1]:
                for room in Metrc_Rooms:
                    if (roomname is not None) and (roomname in room):
                        roomname = room
            if roomname in room_counts.keys():
                count = room_counts[roomname]
                heatmap[column][row + 1].value = count

    # Save Heatmap
    if "Old" in filename:
        heatmap_wb.save("Old " + Output_Filename)
    else:
        heatmap_wb.save(Output_Filename)

Path(Inventory_Filename).expanduser().absolute().rename(f"data/Reports/{date} "
                                                        + Inventory_Filename)
