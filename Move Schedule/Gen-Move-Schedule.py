#! /usr/bin/env python3

"""Generates weekly schedules."""

import datetime
import json

import openpyxl


def getNextMondaysDate(Separator='.'):
    """Gets the date for the next occurring Monday."""
    # ((Day - Today) + 7) % 7 give the weekday index
    Days_After = 7
    today = datetime.date.today()
    days_ahead = Days_After - today.weekday()
    next_monday = today + datetime.timedelta(days=days_ahead)

    return next_monday


def main():
    # TGS Constants
    """
    Phases = (4, 3, 2, 1)
    Rows = "ABCDEFGHIJKLMNOPQRS"
    Four_Tiers = "ABCDEFG"
    Two_Tiers = "HIJK"
    Tiers = (1, 2, 3, 4, 5)
    ONLY IF LOADING ROOMS DOESN'T WORK!
    match = Room_Pattern.search(last_room)
    while match is None:
        print("Error, room not recognized.")

        last_room = input("Enter the last room placed: ")
        match = Room_Pattern.search(last_room)
    Last_Phase = int(match.group(1))
    Last_Row = match.group(2)
    Last_Tier = match.group(3)
    """

    current_date = getNextMondaysDate()
    date = datetime.date.strftime(current_date, "%m.%d.%Y")
    Rooms_Json = "data/Bloom.json"
    TraysperRoom_Json = "data/Trays per Room.json"
    Empty_Schedule = "data/Move Schedule (Empty).xlsx"
    Output_Schedule = f"{date} Move Cycle.xlsx"
    with open("data/Trays per Day.json") as json_file:
        Trays_per_Day = json.load(json_file)

    # Template Constants
    Sheetnames = ("Monday", "Tuesday",
                  "Wednesday", "Thursday",
                  "Friday")
    Room_Col = 'A'
    Tray_Col = 'B'
    Count_Col = 'C'
    Info_Col = 'G'
    Start_Row = 1  # index, not actual row
    Date_Row = 1
    PlantsperTray_Row = 4
    Total_Index = 12
    Total_Header = "TOTAL"
    Blanks = (None, "")

    # Load rooms
    Rooms = dict()
    with open(Rooms_Json) as json_file:
        Rooms = json.load(json_file)

    # Load trays per room
    Trays = dict()
    with open(TraysperRoom_Json) as json_file:
        Trays = json.load(json_file)

    # Get last room placed
    Input_Room_Msg = "Enter the last room placed at the end of this week: "
    last_room = input(Input_Room_Msg).upper()

    # Check if the previous room was a partial
    # and determine if trays still need to be placed
    remaining_trays = int()
    while True:
        full = input("Was it a full room? Y/n: ")
        if (full == 'n') or (full == 'N'):
            Input_Trays_Msg = "Enter the number of trays already placed: "
            trays_placed = int(input(Input_Trays_Msg))
            remaining_trays = int(Trays[last_room]) - trays_placed
            break
        elif (full == 'Y') or (full == 'y'):
            remaining_trays = 0
            break
        else:
            print("Option not recognized. Trying again...")

    # Get plants per tray
    plants_per_tray = int(input("Enter the plants per tray: "))
    totals = list(list())
    schedule_sheets = list(list())
    for sheetname in Sheetnames:
        rooms = list()
        tray_counts = list()
        plant_counts = list()
        total_trays = 0
        total_plants = 0

        # Day/Sheet Setup
        total_remaining_trays = Trays_per_Day

        # Add remaining trays from previous partial
        if (remaining_trays > 0):
            plant_count = remaining_trays * plants_per_tray

            rooms.append(last_room)
            tray_counts.append(remaining_trays)
            plant_counts.append(plant_count)

            total_trays += remaining_trays
            total_plants += plant_count
            total_remaining_trays -= remaining_trays
            remaining_trays = 0

        while True:  # total_remaining_trays > 0:
            # Get index of last room used
            lastroom_index = Rooms.index(last_room)

            # Test at the end of the list
            if lastroom_index == (len(Rooms) - 1):
                room_index = 0  # if so start at the beginning
            else:
                room_index = lastroom_index + 1  # else go to the next index

            # Build rooms, tray counts, and plant counts
            room = Rooms[room_index]
            remaining_trays = int(Trays[room])

            # Determine if remaining trays in room are greater than
            # the number of total remaining trays
            difference = total_remaining_trays - remaining_trays
            if difference >= 0:
                plant_count = remaining_trays * plants_per_tray
                tray_count = remaining_trays
                total_remaining_trays -= remaining_trays

            else:
                plant_count = total_remaining_trays * plants_per_tray
                tray_count = total_remaining_trays
                total_remaining_trays = 0
                remaining_trays = int(Trays[room]) - tray_count

            # Add rooms, tray counts, and plant counts to spreadsheet
            rooms.append(room)
            tray_counts.append(tray_count)
            plant_counts.append(plant_count)

            # Update total trays and plants for the day
            total_trays += tray_count
            total_plants += plant_count

            last_room = room
            room_index += 1

            if total_remaining_trays <= 0:
                break

        # tray_counts.append(total_trays)
        # plant_counts.append(total_plants)
        totals.append([total_trays, total_plants])
        schedule_sheets.append([rooms, tray_counts, plant_counts])

    # Open empty workbook
    # optimize? read_only?
    schedule_wb = openpyxl.load_workbook(Empty_Schedule)
    # Write generated schedule to Excel file
    for index, sheetname in enumerate(Sheetnames):
        schedule = schedule_wb[sheetname]

        rooms = schedule_sheets[index][0]
        trays = schedule_sheets[index][1]
        plants = schedule_sheets[index][2]

        # Add rooms, trays, and counts
        row = Start_Row
        for room in rooms:
            schedule[Room_Col][row].value = room
            row += 1
        row = Start_Row
        for tray_count in trays:
            schedule[Tray_Col][row].value = tray_count
            row += 1
        row = Start_Row
        for plant_count in plants:
            schedule[Count_Col][row].value = plant_count
            row += 1

        # Add totals
        schedule[Tray_Col][Total_Index].value = totals[index][0]
        schedule[Count_Col][Total_Index].value = totals[index][1]

        # Add date
        date = datetime.date.strftime(current_date, "%m/%d/%Y")
        schedule[Info_Col][Date_Row].value = date

        # Add plants per tray
        schedule[Info_Col][PlantsperTray_Row].value = plants_per_tray

        # Delete blank lines

        Next_Row = Start_Row + len(rooms)

        # Count blank lines
        row = Next_Row
        value = schedule[Room_Col][row].value
        blank_count = 0
        while value != Total_Header:
            if value in Blanks:
                blank_count += 1
            row += 1
            value = schedule[Room_Col][row].value

        # Delete blank rows
        if blank_count > 0:
            schedule.delete_rows((Next_Row + 1), blank_count)  # (row, n)
            # if blank_count == 0, then it deletes everything below,
            # including the Totals row

        # Increment date
        current_date += datetime.timedelta(days=1)

    schedule_wb.save(Output_Schedule)
    print(f"Output saved to {Output_Schedule}")

    return


if __name__ == "__main__":
    main()
