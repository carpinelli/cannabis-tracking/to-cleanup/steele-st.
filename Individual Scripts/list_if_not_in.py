#! /usr/bin/env python3

# Libraries
import os
import csv

try:
    import medvs  # if package is installed

except ImportError as current_exception:
    print(current_exception)
    print("medvs is not installed")
    print("Running as portable")

    import sys
    sys.path.append(os.path.join(os.path.dirname(sys.argv[0]),
                                 ".."))
    import medvs


# Variables
CSV_Extension = ".csv"
inputList = []
metrcList = []
destroyed = []
duplicates = []
seen = set()

# Brief Explain, Get filename
print("This script takes a CSV file as input and checks if the values")
print("in Column A of the CSV file are found in Column B. Any values")
print("in Column A that cannot be found in Column B will be printed")
print("to the terminal and saved in 'Output Files'.")
print()
print("Open and check your input CSV file, and make sure a notorious")
print("Excel bug didn't save A and B as one column. Exporting instead")
print("of saving the CSV is recommended for each 'save'. Good luck!")
print()
print("Please copy the input CSV file to the same folder as this script...")


""" MYOWN FUNCTION ADDITION
# Add '.csv' to filename, if not already added
if not filename.endswith(CSV_Extension):
    filename += CSV_Extension
"""


# Generate Script and File Paths
# Path
input_file = input("Drag the input file into this window and press enter: ")
input_file = input_file.strip("'\" ")
if not medvs.path.exists(input_file):
    basename = os.path.basename(input_file)
    input_file = os.path.join(medvs.path.Working_Directory, basename)
    if not medvs.path.exists(input_file):
        input_file = os.path.join(medvs.path.Output_Directory, basename)
        if not medvs.path.exists(input_file):
            raise Exception

destroyed_file = os.path.join(medvs.path.Output_Directory, "Destroyed.csv")
duplicates_file = os.path.join(medvs.path.Output_Directory, "Duplicates.csv")


# Get Data from CSV Columns A and B
with open(input_file, 'r') as csvIn:
    csvreader = csv.reader(csvIn)
    rows = list(csvreader)

    # Get Column A
    for row in rows:
        if row[0] not in (None, ""):
            inputList.append(row[0])

    # Get Column B
    for row in rows:
        if row[1] not in (None, ""):
            metrcList.append(row[1])


# Check each rfid in inputList against metrcList
# Print and save if not found
for row_number, rfid in enumerate(inputList, 1):
    if rfid not in metrcList:
        print("{rfid} not found Column B".format(rfid=rfid))
        destroyed.append(rfid)

    if rfid not in seen:
        seen.add(rfid)
    else:
        print("Row {row}, Found duplicate found: {duplicate}".format(
            duplicate=rfid, row=row_number))
        duplicates.append([rfid, row_number])


# Write the destroyed to file
if len(destroyed) > 0:
    with open(destroyed_file, 'w', newline='') as csv_out:
        writer = csv.writer(csv_out)
        writer.writerows([[row] for row in destroyed])

# Write duplicates to file
if len(duplicates) > 0:
    with open(duplicates_file, 'w', newline='') as csv_out:
        writer = csv.writer(csv_out)
        writer.writerows([row for row in duplicates])


# Notify User of Results
print("If anything found, results are saved to 'Output Files'")
print("Close the window when done!")
