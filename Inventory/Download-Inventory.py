#! /usr/bin/env python3

"""A bot to download information from METRC."""


import time
from datetime import date
from pathlib import Path

from selenium import webdriver
from selenium.common import exceptions as Exceptions
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


username = "m56319"
password = "Kittsboys#1"
email = "nolan.kitts@tgscolorado.com"


# geckodriver @ https://github.com/mozilla/geckodriver/releases


"""Issues:
   Uses time.sleep(n) instead of selenium waits
   Checks if logged in with URL
"""
"""
import bcrypt

password = b"super secret password"

# Hash a password for the first time, with a certain number of rounds
hashed = bcrypt.hashpw(password, bcrypt.gensalt(14))

# Check that a unhashed password matches one that has previously been
#   hashed
if bcrypt.checkpw(password, hashed):
    print("It Matches!")
else:
    print("It Does not Match :(")

# Longer than 72 Characters/Bytes

password = b"an incredibly long password" * 10
hashed = bcrypt.hashpw(
        base64.b64encode(hashlib.sha256(password).digest()),
        bcrypt.gensalt()
        )
"""

"""
report_element.find_element_by_xpath(
"//option[contains(text(), '- All Growth Phases -')]")
"""

Timeout = 10
Default_Wait = Timeout

Mime_Types = ["text/plain", "application/vnd.ms-excel",
              "application/x-msexcel",
              "application/x-excel", "application/excel"
              "text/csv", "application/csv",
              "text/comma-separated-values", "application/download",
              "application/octet-stream", "binary/octet-stream",
              "application/binary", "application/x-unknown"]


class MetrcBot:
    def __init__(self, username, password, email):
        # Account Information
        self.username = username
        self.password = password
        self.email = email

        # URLs
        self.Login = "/log-in?ReturnUrl=%2f"
        self.URL = "https://co.metrc.com"
        self.User = f"{self.URL}/user/profile?licenseNumber=403R-00020"
        self.Homepage = f"{self.URL}/industry/403R-00020/plants"
        self.Reports = f"{self.URL}/industry/403R-00020/reports"

        # Path
        self.Download_Path = Path(".").expanduser().absolute()
        self.Downloads = str(self.Download_Path)

        # FIX XPATH
        self.User_XPath = f"//*[contains(text(), '{self.User}')]"

        # Firefox Preferences
        profile = webdriver.FirefoxProfile()
        mime_types = f"{Mime_Types[1]},{Mime_Types[5]},{Mime_Types[6]}"
        binary = FirefoxBinary("data/geckodriver")

        profile.set_preference("browser.download.folderList", 2)
        profile.set_preference("browser.download.dir", self.Downloads)
        # profile.set_preference("browser.download.alertOnEXEOpen", False)
        # profile.set_preference("browser.download.manager.alertOnEXEOpen",
        #                       False)
        profile.set_preference("browser.helperApps.neverAsk.saveToDisk",
                               mime_types)
        profile.set_preference("browser.download.manager.showWhenStarting",
                               False)
        profile.set_preference("browser.download.manager.focusWhenStarting",
                               False)
        profile.set_preference("browser.helperApps.alwaysAsk.force",
                               False)
        profile.set_preference("browser.download.manager.closeWhenDone",
                               False)
        profile.set_preference("browser.download.manager.closeWhenDone",
                               False)
        profile.set_preference("browser.download.manager.showAlertOnComplete",
                               False)
        profile.set_preference("browser.download.manager.useWindow",
                               False)
        profile.set_preference("browser.download.manager.showWhenStarting",
                               False)
        profile.set_preference("services.sync.prefs.sync.browser.download"
                               ".manager.showWhenStarting",
                               False)
        profile.set_preference("pdfjs.disabled", True)

        # Start Browser
        self.bot = webdriver.Firefox(profile,
                                     executable_path="data/geckodriver",
                                     service_log_path="data/geckodriver.log")

    def isLoggedIn(self):
        try:
            self.bot.find_element_by_class_name("icon-user")

        except Exceptions.NoSuchElementException:
            return False

        else:
            return True

    def login(self):
        bot = self.bot

        bot.get(self.URL + self.Login)

        username = bot.find_element_by_id("username")
        password = bot.find_element_by_id("password")
        email = bot.find_element_by_id("email")

        username.clear()
        password.clear()
        email.clear()

        username.send_keys(self.username)
        password.send_keys(self.password)
        email.send_keys(self.email, Keys.RETURN)

        # Wait for login to load
        # WebDriverWait(bot, Timeout).until(EC.url_changes(self.URL
        #                                                  + self.Login))
        time.sleep(Default_Wait)

    def downloadReport(self, Type="Inventory", Date_Range=12):
        bot = self.bot

        # check if logged in
        if not self.isLoggedIn():
            print("AssertionError: Must be logged in!")
            raise AssertionError

        bot.get(self.Reports)

        # FIND AND CLICK REPORTS TAB

        wait = WebDriverWait(bot, 10)
        startdate = wait.until(
            EC.presence_of_element_located(
                (By.XPATH,
                 "//input[@ng-model='line.PlantsInventoryStartDate']")))
        enddate = wait.until(
            EC.presence_of_element_located(
                (By.XPATH,
                 "//input[@ng-model='line.PlantsInventoryEndDate']")))

        startdate.clear()
        enddate.clear()

        todays_date = date.strftime(date.today(), "%m/%d/%Y")
        last_year = todays_date[:-1] + str(int(todays_date[-1]) - 1)

        startdate.send_keys(last_year)
        enddate.send_keys(todays_date)

        excel_xpath = ("//button[@ng-click=\"preload.methods."
                       "viewPlantsInventoryReport(line, 'excel');\"]")
        download_report = bot.find_element_by_xpath(excel_xpath)
        download_report.click()

        # AUTOGUI CLICK OKAY

        # SAVE
        # "//*[contains(text(), '{self.User}')]")


ops = MetrcBot(username, password, email)
# HASHME ASSHOLE!

ops.login()
if not ops.isLoggedIn():
    print("ERROR! Login Failed...")
    ops.bot.quit()
    exit(1)

ops.downloadReport()
input("Press enter when the report has finished downloading...")
ops.bot.quit()
